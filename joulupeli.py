import pygame, sys, os
from pygame.locals import *

print(os.path.realpath("joulupeli.py"))

os.environ['SDL_VIDEO_CENTERED'] = '1'

pygame.init()

winWidth = 300
winHeight = 300

DISPLAYSURF = pygame.display.set_mode((winWidth,winHeight))
pygame.display.set_caption("Hyvää joulua!")

hero = pygame.image.load("pictures/pienilammas.png") # Hero in 2 directions
realHero = hero # Is the hero facing right or left?
heroY = 100

flock = pygame.image.load("pictures/lauma.png")
background1 = pygame.image.load("pictures/tausta1.png")
background2 = pygame.image.load("pictures/tausta2.png")

pygame.mixer.music.load("music/bbc_sheep.wav")
pygame.mixer.music.play(loops = -1, start = 0.0)

class bg:
    x = 0
    y = 0
    min = 0
    max = 0

bg1 = bg()
bg1.x = -2920
bg1.min = -2920
bg1.max = 0

bg2 = bg()
bg2.x = -1320
bg2.min = -1320
bg2.max = 1587

flockSpot = bg()
flockSpot.x = -2920
flockSpot.y = 70
flockSpot.min = -2920
flockSpot.max = 0


def quitGame():
    pygame.quit()
    sys.exit()

def moveLeft(bgs,hero):
    for bg in bgs:
        if bg.x < bg.max:
            bg.x += 40
    return bgs,hero

def moveRight(bgs,hero):
    for bg in bgs:
        if bg.x > bg.min:
            bg.x -= 40
    return bgs,(pygame.transform.flip(hero,1,0)) #hero gets flipped when walking right



pygame.key.set_repeat(1,100)
keyDict= {
    K_LEFT : moveLeft, # K_left tunnistaa unicoden.
    K_RIGHT : moveRight
}

basicDict = {
    QUIT : quitGame,
}



while True:

    DISPLAYSURF.blit(background1,(bg1.x,bg1.y))
    DISPLAYSURF.blit(background2,(bg2.x,bg2.y))
    DISPLAYSURF.blit(flock,(flockSpot.x,flockSpot.y))
    DISPLAYSURF.blit(realHero,(50,heroY))
    pygame.mixer.music.set_volume(1-(bg1.x/bg1.min))

    for event in pygame.event.get():
        if event.type in basicDict:
            basicDict[event.type]()
        elif event.type == KEYDOWN:
            if heroY == 100:
                heroY = 110
            else:
                heroY = 100
            if event.key in keyDict:
                (flockSpot,bg1,bg2),realHero = keyDict[event.key]((flockSpot,bg1,bg2),hero)

        pygame.display.update()